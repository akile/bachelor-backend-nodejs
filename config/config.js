module.exports = {
    user          : 'logbook_ti_dev',
    password      : process.env['databasepassword'],
    connectString : `(DESCRIPTION=(ADDRESS= (PROTOCOL=TCP) (HOST=devdb11-s.cern.ch) (PORT=10121) )(ENABLE=BROKEN)(CONNECT_DATA=(SERVICE_NAME=devdb11_s.cern.ch)))`
    
}