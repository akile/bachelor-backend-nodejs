const express = require('express');
const router = express.Router();
const db_api = require('../api/v1/database_api');

const swaggerUi = require('swagger-ui-express');
const auth = require('../api/v1/access/auth');


const swaggerJSDoc = require('swagger-jsdoc');

const options = {
  definition: {
    openapi: '3.0.2',
    info: {
      title: 'API documentation',
      description: 'This is a sample API documentation just to show simple CRUD methods',
      version: '0.1',
    },
  },
  apis: ['./routes/users.js', './routes.index.js'],
};

const OpenAPIspec = swaggerJSDoc(options);

router.use('/docs', swaggerUi.serve, swaggerUi.setup(OpenAPIspec));

router.get('/api-docs.json', (req, res) => {
  res.setHeader('Content-Type', 'application/json')
  res.send(OpenAPIspec)
})

router.get('/', (req, res) => {
  res.render('index')
});

/**
 * Create schemas in order to reuse objects, primitives or arrays. Defines input and output data types.
 * 
 * @swagger
 * components:
 *  schemas:
 *    User:
 *      type: array
 *      properties:
 *        username:
 *          type: string
 *        id:
 *          type: integer
 *          format: int64
 *  
 */


 /**
  * Add tags to the documentation in order to easier indetify the HTTP-methods connected to the different functions
  * 
  * @swagger
  * tags:
  *   name: User
  *   description: CRUD methods for different methods for an user.
  */


/**
 * @swagger
 * /users:
 *  get:
 *    description: Returns all users from the system
 *    summary: Get every user in the system
 *    tags: [User, All]
 *    responses:
 *      '200':
 *        description: A list of users
 *        content:
 *          application/json:
 *            schema:
 *              type: array
 *              items:
 *                $ref: '#/components/schemas/user'
 *    
 */
router.get('/users', (req, res) => {
  let result = [];
  try {
    Promise.resolve(db_api.getUsers())
    .then((values) => {
      for(let i = 0; i < values.length; i++) {
        let user = {
          ID: values[i].ID,
          USERNAME: values[i].USERNAME,
          EMAIL: values[i].EMAIL
        }
        result.unshift(user)
      }
    })
    .then(() => {
      res.send(result)
    })
    .catch((err) => {
      console.log(err.message);
    })
  } catch (err) {
    res.end(err.message);
  }
})

router.get('/users/current', (req, res) => {
  try{
    Promise.resolve(auth.init())
    .then((value) => {
      res.send(value)
    })
    .catch((err) =>{
      console.log(err.message);
    })
  } catch (err) {
    res.end(err.message);
  } 
})

router.get('/users/person', (req, res) => {
  try {
    Promise.resolve(auth.person(req.get('X-Remote-User')))
    .then((value) => {
      res.sendStatus(value);
    })
    .catch((err) => {
      console.log(err.message);
    })
  } catch (err) {
    res.end(err.message)
  }
})
/**
 * @swagger
 * /users/{id}:
 *  get:
 *    description: Gets a user by ID
 *    summary: Find users by ID
 *    tags: [User, All]
 *    operationId: getUserById
 *    parameters:
 *      - name: id
 *        in: path
 *        description: The unique ID of an user
 *        required: true
 *        schema:
 *          type: integer
 *          format: int64
 *          items:
 *            $ref: '#/components/schemas/user'
 *    responses:
 *      '200':
 *        description: Successful operation
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/user'
 *
 */

router.get('/users/:id',(req, res) => {
  if(req.params.id){
    try {
      Promise.resolve(db_api.getUserById(req.params.id))
      .then((result) => {
        console.log(result)
        res.send(result)
      });
    } catch (err) {
      res.end(err.message);
    }
  }
})


/**
 * @swagger
 * /users:
 *  post:
 *    description: Create a new user
 *    tags: [User, All]
 *    summary: Create a new user
 *    parameters:
 *      - in: header
 *        name: username
 *        required: true
 *        schema:
 *          type: string
 *          description: The username of the new user
 *      - in: header
 *        name: email
 *        required: false
 *        schema:
 *          type: string
 *          description: The email of the new user
 *    responses:
 *      '201':
 *        description: Created new user
 *        content: 
 *          application/json:
 *            schema: 
 *              description: Return value is the new ID of the user
 *              type: integer
 *              format: int64
 * 
 */

router.post('/users', (req, res) => {
  if(req.body){
    try{
      newUser = {
        id: '',
        username: req.body.USERNAME,
        email: req.body.EMAIL,
      }
      console.log(newUser.username + ' ' + newUser.email)
      Promise.resolve(db_api.addUser(newUser.username, newUser.email))
      .then((response) => {
        res.send(response.outBinds.userId);
      })
    } catch (err) {
      res.end(err.message)
    }
  }
})

/**
 * @swagger
 * /users/{id}:
 *  delete:
 *    description: Delete user by providing an user ID
 *    tags: [User, All]
 *    summary: Delete user
 *    parameters:
 *      - name: id
 *        in: path
 *        description: The unique ID of an user
 *        required: true
 *        schema:
 *          type: integer
 *          format: int64
 *    responses:
 *      '200':
 *        description: Successful operation
 *        content: 
 *          application/json:
 *            schema: 
 *              $ref: '#/components/schemas/user'
 *        
 *        
 *    
 */

router.delete('/users/:id', (req, res) => {
  if(req.params.id) {
    let userId = parseInt(req.params.id)
    try{
      res.send(Promise.resolve(db_api.deleteUser(userId)))
    } catch (err) {
      res.end(err.message)
    }
  }
})

/**
 * @swagger
 * /users/{id}:
 *  put:
 *    description: Update user
 *    tags: [User, All]
 *    summary: Update user by the users ID
 *    parameters:
 *      - name: ID
 *        in: path
 *        description: The unique ID of an user
 *        required: true
 *        schema:
 *          type: integer
 *          format: int64
 *    responses:
 *      '200':
 *        description: Successfully updated user
 *        content: 
 *          application/json:
 *            schema: 
 *              $ref: '#/components/schemas/User'
 *        
 *        
 *    
 */

router.put('/users/:id', (req, res) => {
  let userId = req.params.id
  if(userId) {
      let updatedUser = {
        newUsername: req.body.USERNAME,
        newEmail: req.body.EMAIL,
        userId: req.body.ID
      }
      try {
        Promise.resolve(db_api.updateUser(updatedUser))
        .then((response) => {
          res.send(response.data)
          console.log(updatedUser.newUsername + " successfully updated!")

        })
      } catch (err) {
        res.send(err.message)
      }
  }
})

module.exports = router;


