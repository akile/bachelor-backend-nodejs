FROM gitlab-registry.cern.ch/linuxsupport/cc7-base

RUN yum install -y \
    gcc-c++ make \
    libldap2-dev lldap\
    nodejs

# Install the Oracle instant client.
RUN yum -y --enablerepo cernonly install oracle-instantclient11.2-basiclite oracle-instantclient11.2-meta oracle-instantclient-tnsnames.ora.noarch    

RUN mkdir /opt/backend
RUN cd /opt/backend

COPY *.json /opt/backend/

WORKDIR /opt/backend

RUN npm install -y

COPY . /opt/backend

EXPOSE 8000

CMD ["npm", "start"] 