var oracledb = require('oracledb');
var db_config = require('../../config/config')

oracledb.outFormat = oracledb.OBJECT


/**
 *  /GET method
 * 
 * Retrieve every users in database
 */

function getUsers() {
    return new Promise((resolve, reject) => {
        let conn

        oracledb
        .getConnection(db_config)
        .then((c) => {
            console.log('Database connected... Fetching users..')

            conn = c

            return conn.execute(
                `SELECT * 
                FROM USER_TEST`
            )
        })
        .then((result) => {
            console.log('Query executed... Users fetched')
            resolve(result.rows)
        }, (err) => {
            console.log('An error occured: ', err)
            reject(err)
        })
        .then(() => {
            if(conn){
                console.log('Closing connection to database...')
                return conn.close()
            }
        })
        .then(() => {
            console.log('Connection closed...')
        })
        .catch((err) => {
            if(err){
                console.log('Error during closing connection: ', err)
            }
        })
    })
}

function getUserById(id) {
    return new Promise((resolve, reject) => {
        let conn

        oracledb
        .getConnection(db_config)
        .then((c) => {
            console.log('Database connected...')
            conn = c;
            return conn.execute(
                `SELECT * 
                FROM USER_TEST
                WHERE ID = :id`,
                [id],
                {autoCommit: true}
            )
        })
        .then((result) => {
            console.log('Query executed... User ' + (id) + ' fetched')
            resolve(result.rows[0])
        }, (err) => {
            console.log('An error occured: ', err)
            reject(err)
        })
        .then(() => {
            if(conn){
                return conn.close()
            }
        })
        .then(() => {
            console.log('Connection closed...')
        })
        .catch((err) => {
            if(err){
                console.log('Error during closing connection: ', err)
            }
        })
    })
}

function addUser(username, email) {
    return new Promise((resolve, reject) => {
        let conn;

        oracledb
        .getConnection(db_config)
        .then((c) => {
            console.log('Database connected...');
            console.log('Executing query...');

            conn = c;
            return conn.execute(
                `INSERT INTO USER_TEST 
                (USERNAME, EMAIL) 
                VALUES (:USERNAME, :EMAIL)
                RETURNING ID
                INTO :userId`,
                {
                    username: username, 
                    email: email,
                    userId: {
                        type: oracledb.NUMBER,
                        dir: oracledb.BIND_OUT,
                    }
                },
                {
                    autoCommit: true,
                }
            )
        })
        .then((result) => {
            resolve(result.outBinds.userId)
        }, (err) => {
            console.log('An error occured: ', err)
            reject(err)
        })
        .then(() => {
            if(conn){
                return conn.close()
            }
        })
        .then(() => {
            console.log('Connection closed...')
        })
        .catch((err) => {
            if(err){
                console.log('Error during closing connection: ', err)
            }
        })
    })
}


function deleteUser(userId) {
    return new Promise((resolve, reject) => {
        let conn;

        oracledb
        .getConnection(db_config)
        .then((c) => {
            console.log('Database connected...')

            conn = c

            return conn.execute(
                `DELETE FROM USER_TEST 
                WHERE ID = :userId`,
                [userId],
                {autoCommit: true}
            )
        })
        .then((result) => {
            console.log('UserId ' + userId + ' deleted successfully')
            resolve(result)
        }, (err) => {
            console.log('An error occured: ', err)
            reject(err)
        })
        .then(() => {
            if(conn){
                return conn.close()
            }
        })
        .then(() => {
            console.log('Connection closed...')
        })
        .catch((err) => {
            if(err){
                console.log('Error during closing connection: ', err)
            }
        })
    })
}

function updateUser(user) {
    return new Promise((resolve, reject) => {
        let conn;
        
        oracledb
        .getConnection(db_config)
        .then((c) => {
            console.log('Database connected...Updating user...')

            conn = c;

            return conn.execute(
                `UPDATE USER_TEST
                SET USERNAME = :newUsername,
                EMAIL = :newEmail
                WHERE ID = :userId`,
                [user.newUsername, user.newEmail, user.userId],
                {autoCommit: true}
            )
        })
        .then((result) => {
            resolve(result)
        }, (err) => {
            console.log('An error occured: ', err)
            reject(err)
        })
        .then(() => {
            if(conn){
                return conn.close()
            }
        })
        .then(() => {
            console.log('Connection closed...')
        })
        .catch((err) => {
            if(err){
                console.log('Error during closing connection: ', err)
            }
        })
    })
}

module.exports.getUsers = getUsers
module.exports.addUser = addUser
module.exports.deleteUser = deleteUser
module.exports.updateUser = updateUser
module.exports.getUserById = getUserById