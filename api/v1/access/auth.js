const ldap = require('ldapjs');

let client = ldap.createClient({
    url: 'ldap://xldap.cern.ch'
})

const CERN_USER_BASE = 'ou=Users,ou=Organic Units,dc=cern,dc=ch';
const CERN_GROUP_BASE = 'OU=e-groups,OU=Workgroups,DC=cern,DC=ch';
// Used to query if an account is a member of a specific e-group. Arguments are the account name, the group name, and CERN_GROUP_BASE
const CERN_GROUP_MEMBERSHIP_QUERY = '(&(objectClass=user)(sAMAccountName=%s)(memberOf:1.2.840.113556.1.4.1941:=CN=%s,%s))';
// Used to query all the e-groups of an account. Arguments are ROLE_NAME_PREFIX, the account name, and CERN_USER_BASE
const CERN_ACCOUNT_GROUPS_QUERY = '(&(CN=%s*)(member=CN=%s,%s))';
// Used to query the properties of an account. Arguments are the account name, and CERN_USER_BASE
const CERN_ACCOUNT_QUERY = '(&(objectClass=user)(uidNumber=*)(msExchHideFromAddressLists=FALSE)(cernaccounttype=Primary)(!(division=Other))(CN=%s))';
// Used to query by first or last name.
const CERN_NAME_QUERY_SIMPLE = '(&(objectClass=user)(uidNumber=*)(cernaccounttype=Primary)(!(division=Other))(|(sn=%s)(givenName=%s)))';
// Used to query by first and last name.
const CERN_NAME_QUERY_FIRST_AND_LAST = '(&(objectClass=user)(uidNumber=*)(cernaccounttype=Primary)(!(division=Other)) (|(&(sn=%s)(givenName=%s))(&(givenName=%s)(sn=%s))) )';


function person(username){
    const search_options = {
        filter: `(&(objectClass=user)(uidNumber=*)(msExchHideFromAddressLists=FALSE)(cernaccounttype=Primary)(!(division=Other))(CN=${username}))`,
        scope: 'sub',
        attributes: 'name'
    }

    return new Promise((resolve, reject) => {
        client.search(CERN_USER_BASE, search_options, (err, res) => {
            if(err) {
                console.log('Error occured during LDAP search');
                reject(err)
            } else {
                res.on('searchEntry', (entry) => {
                    console.log('Entry', JSON.stringify(entry.object));
                    resolve(entry.object);
                })
                res.on('searchReference', (referral) => {
                    console.log('Referral', referral.uris.join());
                    resolve(referral.uris.join());
                })
                res.on('error', (error) => {
                    console.log('Error', error.message);
                    reject(error);
                })
                res.on('end', (result) => {
                    console.log('status: ', result);
                    resolve(result);
                })
            }
        })  
    })
}


module.exports.person = person;